using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerOnePaddleMovement : MonoBehaviour
{
    public float moveSpeed;
    public float topBounds;
    public float bottomBounds;
    public Vector2 startingPos = new Vector2(6.95f, 0);
    private GameObject ball;
    private Vector2 ballPos;
    // Start is called before the first frame update
    void Start()
    {
        transform.localPosition = (Vector3)startingPos;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }
    void Move()
    {
        if(!ball)
        {
            ball = GameObject.FindGameObjectWithTag("Ball");
        }
        if (ball.GetComponent<Ball>().ballDirection == Vector2.right)
        {
            ballPos = ball.transform.localPosition;
            if(transform.localPosition.y > bottomBounds && ballPos.y < transform.localPosition.y)
            {
                transform.position = new Vector3(0, -moveSpeed * Time.deltaTime, 0);
            }
            if(transform.localPosition.y < bottomBounds && ballPos.y > transform.localPosition.y)
            {
                transform.position = new Vector3(0, moveSpeed * Time.deltaTime, 0);
            }
        }
    }
}
