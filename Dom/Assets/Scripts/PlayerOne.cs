using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerOne : MonoBehaviour
{
    public float moveSpeed;
    public float topBounds;
    public float bottomBounds;
    public Vector2 startingPos = new Vector2(-4.95f, 0);
    // Start is called before the first frame update
    void Start()
    {
        transform.localPosition = (Vector3)startingPos;
    }

    // Update is called once per frame
    void Update()
    {
        CheckUserInput();
    }
    void CheckUserInput()
    {
        if(Input.GetKey(KeyCode.W))
        {
            if(transform.position.y > topBounds)
            {
                transform.localPosition = new Vector3(transform.localPosition.x, topBounds, transform.localPosition.z);
            }
            else
            {
                transform.localPosition += Vector3.up * moveSpeed * Time.deltaTime;
            }
        }
        else if(Input.GetKey(KeyCode.S))
        {
            if (transform.position.y < bottomBounds)
            {
                transform.localPosition = new Vector3(transform.localPosition.x, bottomBounds, transform.localPosition.z);
            }
            else
            {
                transform.localPosition += Vector3.down * moveSpeed * Time.deltaTime;
            }
        }
    }
}
