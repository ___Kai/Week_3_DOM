using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public float movementSpeed;
    public Vector2 ballDirection = Vector2.left;
    public float leftPaddleHeight, leftPaddleWidth, rightPaddleHeight, rightPaddleWidth, leftPaddleMaxX, leftPaddleMaxY, leftPaddleMinX, leftPaddleMinY, rightPaddleMaxX, rightPaddleMaxY, rightPaddleMinX, rightPaddleMinY, ballHeight, ballWidth;
    private GameObject leftPaddle, rightPaddle;
    private float bounceAngle;
    private float vx, vy;
    public float maxAngle;
    public float topBounds;
    public float bottomBounds;
    private bool collidedWithLeft, collidedWithRight, collidedWithWall;
    // Start is called before the first frame update
    void Start()
    {
        if(movementSpeed < 0)
        {
            movementSpeed = -1 * movementSpeed;
        }
        leftPaddle = GameObject.Find("left_paddle");
        rightPaddle = GameObject.Find("right_paddle");

        leftPaddleHeight = leftPaddle.transform.GetComponent<SpriteRenderer>().bounds.size.y;
        leftPaddleWidth = leftPaddle.transform.GetComponent<SpriteRenderer>().bounds.size.x;
        rightPaddleHeight = rightPaddle.transform.GetComponent<SpriteRenderer>().bounds.size.y;
        rightPaddleWidth = rightPaddle.transform.GetComponent<SpriteRenderer>().bounds.size.x;
        ballHeight = transform.GetComponent<SpriteRenderer>().bounds.size.y;
        ballWidth = transform.GetComponent<SpriteRenderer>().bounds.size.x;
        leftPaddleMaxX = leftPaddle.transform.localPosition.x + leftPaddleWidth / 2;
        leftPaddleMinX = leftPaddle.transform.localPosition.x - leftPaddleWidth / 2;
        rightPaddleMaxX = rightPaddle.transform.localPosition.x - rightPaddleWidth / 2;
        rightPaddleMinX = rightPaddle.transform.localPosition.x + rightPaddleWidth / 2;
        CheckCollision();
        bounceAngle = GetBouncingAngle();
        vx = movementSpeed * Mathf.Cos(bounceAngle);
        vy = movementSpeed * - Mathf.Sin(bounceAngle);
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }
    bool CheckCollision()
    {

        // The top & bottom edges of the paddles in motion:
        leftPaddleMaxY = leftPaddle.transform.localPosition.y + leftPaddleHeight / 2;
        leftPaddleMinY = leftPaddle.transform.localPosition.y - leftPaddleHeight / 2;

        rightPaddleMaxY = rightPaddle.transform.localPosition.y + rightPaddleHeight / 2;
        rightPaddleMinY = rightPaddle.transform.localPosition.y - rightPaddleHeight / 2;

        // check for x collision
        if (transform.localPosition.x - ballWidth / 2 < leftPaddleMaxX && transform.localPosition.x + ballWidth / 2 > leftPaddleMinX)
        {

            // then check for y collision
            if (transform.localPosition.y - ballHeight / 2 < leftPaddleMaxY && transform.localPosition.y + ballHeight / 2 > leftPaddleMinY)
            {
                collidedWithLeft = true;
                return true;
            }
        }

        if (transform.localPosition.x + ballWidth / 2 > rightPaddleMaxX && transform.localPosition.x - ballWidth / 2 < rightPaddleMinX)
        {

            if (transform.localPosition.y - ballHeight / 2 < rightPaddleMaxY && transform.localPosition.y + ballHeight / 2 > rightPaddleMinY)
            {
                collidedWithRight = true;
                return true;
            }
        }
        if(transform.localPosition.y > topBounds)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, topBounds, transform.localPosition.z);
            collidedWithWall = true;
            return true;
        }
        if(transform.localPosition.y < bottomBounds)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, bottomBounds, transform.localPosition.z);
            collidedWithWall = true;
            return true;
        }
        return false;
    }


    void Move()
    {
        if (!CheckCollision())
        {
            vx = movementSpeed * Mathf.Cos(bounceAngle);
            if(movementSpeed > 0)
            {
                vy = movementSpeed * -Mathf.Sin(bounceAngle);
            }
            else
            {
                vy = movementSpeed * Mathf.Sin(bounceAngle);
            }
            ballDirection.x = ballDirection.x * -1;
            transform.localPosition += new Vector3(ballDirection.x * vx * Time.deltaTime, vy * Time.deltaTime, 0);
        }
        else
        {
            if(movementSpeed < 0)
            {
                movementSpeed = -1 * movementSpeed;
                if(collidedWithLeft == true)
                {
                    collidedWithLeft = false;
                    float relativeIntersectY = leftPaddle.transform.localPosition.y - transform.localPosition.y;
                    float normalisedRelativeIntersectY = (relativeIntersectY / leftPaddleHeight / 2);
                    bounceAngle = normalisedRelativeIntersectY * (maxAngle * Mathf.Deg2Rad);
                }
                else if(collidedWithRight == true)
                {
                    collidedWithRight = false;
                    float relativeIntersectY = rightPaddle.transform.localPosition.y - transform.localPosition.y;
                    float normalisedRelativeIntersectY = (relativeIntersectY / rightPaddleHeight / 2);
                    bounceAngle = normalisedRelativeIntersectY * (maxAngle * Mathf.Deg2Rad);
                }
                else if(collidedWithWall == true)
                {
                    collidedWithWall = false;
                    bounceAngle = -bounceAngle;
                }
            }
        }
    }
    float GetBouncingAngle(float minDegrees = 160f, float maxDegrees = 260f)
    {
        float minRad = minDegrees * Mathf.PI / 180;
        float maxRad = maxDegrees * Mathf.PI / 180;
        return Random.Range(minRad, maxRad);
    }
}
